DEBUG=false
JSON=public/perro.json

# Va a la raíz del repo
cd $(dirname $0)/..

# Actualiza el repo
if [ $DEBUG = false ]; then
  git pull --quiet
fi

# Limpia sitio
if [ -d "public" ]; then rm -rf public; fi
mkdir public
cp -r src/* public
rm public/*.*

# Descarga info de linkding
curl --request GET \
     -s -o $JSON \
     -H "Authorization: Token $(<.token)" \
     "https://linkding.cuates.net/api/bookmarks/?q=%23blog"
if [ "$?" != 0 ]; then exit 1; fi

# Rehace sitio y JSON
python3 ./scripts/make.py $JSON

# Hace commit al repo si hubo cambios
if [ $DEBUG = false ] && [ -n "$(git status --porcelain)" ]; then
    git add .
    git commit -m "Actualización"
    git push -q origin
    git push -q cuates
    git log -1 --name-only --oneline | cat
fi

# Limpia variables
unset DEBUG
unset JSON
